<?php
spl_autoload_register();

$definer = new \Core\Config\Definer();

//Include the core
include(BASE_DIR."/Core/Controller.php");
include(BASE_DIR."/Core/Model.php");

//Dispatch
$request = new Request();
$dispatch = new Dispatcher($request);

$dispatch->dispatch();