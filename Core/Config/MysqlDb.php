<?php
namespace Core\Config;


use Interfaces\MysqlInterfaces;

class MysqlDb implements MysqlInterfaces
{

    /**
     * Delete Data From Data Store
     *
     *
     * @param $handler : Database or File Handler
     * @param $condition : which data that you want to delete
     * @return mixed
     */
    public function deleteData($handler, $condition)
    {
        // TODO: Implement deleteData() method.
    }

    /**
     * Delete All Data From Data Store
     *
     *
     * @param $handler : Database or File Handler
     * @return mixed
     */
    public function rollBack($handler)
    {
        // TODO: Implement rollBack() method.
    }

    /**
     * Connect to Mysql and Return the Connection Handler
     * @return mixed
     */
    public function connectToMySql()
    {
        // TODO: Implement connectToMySql() method.
    }

    /**
     * Retrieve Data From Data Store
     *
     *
     * @param $handler : Database or File Handler
     * @param $condition : which data that you want to retrieve
     * @return mixed
     */
    public function retrieveData($handler, $condition)
    {
        // TODO: Implement retrieveData() method.
    }

    /**
     * Store Data in a Data Store
     *
     * @param $handler : Database or File Handler
     * @param $data
     * @return mixed
     */
    public function storeData($handler, $data)
    {
        // TODO: Implement storeData() method.
    }

    /**
     * Update Data in a Data Store
     *
     * @param $handler : Database or File Handler
     * @param $condition : Which data that you want to update
     * @param $data
     * @return mixed
     */
    public function updateData($handler, $condition, $data)
    {
        // TODO: Implement updateData() method.
    }
}