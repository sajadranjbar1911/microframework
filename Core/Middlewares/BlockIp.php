<?php
namespace Core\Middlewares;


use Interfaces\Middleware;

class BlockIp implements Middleware
{
    private $blockIps = [];
    function handler()
    {
        if(array_search($_SERVER['REMOTE_ADDR'] , $this->blockIps) !== false){
            die("Your IP Is Blocked");
        }
    }
}