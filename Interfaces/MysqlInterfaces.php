<?php
/**
 * Created by PhpStorm.
 * User: system-pc1
 * Date: 11/3/2019
 * Time: 12:14 PM
 */

namespace Interfaces;


interface MysqlInterfaces extends StoreData,RetrieveData,UpdateData,DeleteData
{
    /**
     * Connect to Mysql and Return the Connection Handler
     * @return mixed
     */
    public function connectToMySql();
}