<?php
/**
 * Created by PhpStorm.
 * User: system-pc1
 * Date: 11/3/2019
 * Time: 12:10 PM
 */

namespace Interfaces;


interface DeleteData
{
    /**
     * Delete Data From Data Store
     *
     *
     * @param $handler : Database or File Handler
     * @param $condition : which data that you want to delete
     * @return mixed
     */
    public function deleteData($handler , $condition);

    /**
     * Delete All Data From Data Store
     *
     *
     * @param $handler : Database or File Handler
     * @return mixed
     */
    public function rollBack($handler);
}