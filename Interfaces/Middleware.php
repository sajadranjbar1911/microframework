<?php
namespace Interfaces;


interface Middleware
{
    public function handler();
}