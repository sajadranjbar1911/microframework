<?php

class Dispatcher
{
    public $url;
    public $request;

    public function __construct(Request $request)
    {
        //Set request and url
        $this->request = $request;
        $this->url = $request->url;
    }

    public function dispatch()
    {
        //Call All Middlewares
        $this->middlewareProvider();
        //Call Router
        Router::render($this->url, $this->request);
        //Load Controller
        $controller = $this->loadController();

        //Call the controller class and methods with parameters
        call_user_func_array([$controller, $this->request->method], [$this->request->params]);
    }

    public function loadController()
    {
        //Check Conditional Not Found Controller Flag
        $controller = $this->request->notFound ? "NotFoundController" : $this->request->controller;
        //Directory File Address
        $file = BASE_DIR . "App/Controllers/" . $controller . ".php";
        include($file);

        //Namespace File Address
        $controller = "\App\Controllers\\" . $controller;
        return new $controller;

    }

    private function middlewareProvider()
    {
        $files = array_slice(scandir(BASE_DIR . "Core/Middlewares"), 2);
        foreach ($files as $file) {
            $file = str_replace(".php", "", $file);

            $middlewareClass = "Core\Middlewares\\" . $file;
            $middleware = new $middlewareClass;
            if (method_exists($middleware, 'handler')) {
                $middleware->handler();
            }
        }
    }
}